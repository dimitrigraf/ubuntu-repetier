#!/bin/bash

set -euo pipefail

repetierPrinterSlug='CHANGE_PRINTER_SLUG'
repetierApiKey='CHANGE_API_KEY'

curl "http://localhost:3344/printer/api/$repetierPrinterSlug?a=emergencyStop&apikey=$repetierApiKey"

zenity --warning --title="Overpressure Warning" --text "The print process was aborted due to overpressure!" --width=200
