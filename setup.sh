#!/bin/bash

# exit script on error
set -euo pipefail

# save working directory
workingDir="$PWD"

# add foreign apt repos
# anydesk
wget -qO - https://keys.anydesk.com/repos/DEB-GPG-KEY | sudo apt-key add -
echo "deb http://deb.anydesk.com/ all main" | sudo tee /etc/apt/sources.list.d/anydesk-stable.list

# yoctopuce
wget -qO - https://www.yoctopuce.com/apt/KEY.gpg | sudo apt-key add -
echo "deb https://www.yoctopuce.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/yoctopuce.list

# update system and load newly added repos
sudo apt update
sudo apt upgrade -y

# install AnyDesk
sudo apt install -y anydesk

# install notepadqq
sudo apt install -y notepadqq
cp mimeapps.list $HOME/.config/

# prepare mono for Yocto-Visualization
sudo apt install -y mono-complete
# temporary workaround: there's a bug (mem leak) in the libgdiplus 6.0.4 library supplied by the ubuntu repo
# therefore we use the 6.0.5 version from the github repo (https://github.com/mono/libgdiplus)
# local build
tar -xzvf libgdiplus-6.0.5.tar.gz
cd libgdiplus-6.0.5
./autogen.sh --prefix=/usr/
make
sudo make install
# cleanup
cd "$workingDir"
rm -r libgdiplus-6.0.5

# set up VirtualHub and Yocto-Visualization
sudo apt install -y yoctovisualization virtualhub

# required so that writing to the modules is also allowed (see VirtualHub doc)
sudo cp 51-yoctopuce_all.rules /etc/udev/rules.d/

# make VirtualHub and Yocto-Visualization start automatically after boot
mkdir -p $HOME/.config/autostart/
cp VirtualHub.desktop $HOME/.config/autostart/
cp Yocto-Visualization.desktop $HOME/.config/autostart/

# copy Yocto-Visualization config file to proper path
mkdir -p $HOME/.config/Yoctopuce/YoctoVisualization/2.0.0.0/
cp config.xml $HOME/.config/Yoctopuce/YoctoVisualization/2.0.0.0/

# Put kill script in position
mkdir -p $HOME/bin
cp KillRepetierServer.sh $HOME/bin/

# install Repetier Server
sudo apt install -y curl
wget $(curl -s https://download3.repetier.com/files/server/debian-amd64/updateinfo.txt | grep .deb)
sudo dpkg -i Repetier-Server-*-Linux.deb
sudo apt remove -y modemmanager
# cleanup
rm Repetier-Server-*-Linux.deb

# open the server in Firefox automatically after boot
cp firefox.desktop $HOME/.config/autostart/

# set up motion for webcam stream
sudo apt install -y motion
sudo cp motion.conf /etc/motion/
sudo sed -i 's/start_motion_daemon=no/start_motion_daemon=yes/' /etc/default/motion
sudo service motion start

# apt cleanup
sudo apt autoclean
sudo apt autoremove -y
